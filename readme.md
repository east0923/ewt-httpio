# 1.1.0 20180208
今后需要etools v1.1.x版本配合
20180208, 重大更新，对MIME请求、解析使用新语法，需要etools 1.1系列配合
20180205, session管理类中，修复语法错误: w引用错误

# 1.0.6 20180205
20180203, 增添Session校验委托功能，为跨域共享SessionId创造可能
20180131, httpio.js: 修复收到ws消息时，解析出不含json导致崩溃的问题
20180201, httpio.js: 改进cookie读取代码，取消tryCatch语句

# 1.0.5 20171228
20171228, httpio.js: 修复所有try里面包含callback的隐患