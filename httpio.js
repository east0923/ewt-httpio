const HTTP=require('http');
const URL = require('url');
const fs=require('fs');
const path=require('path');
const WebSocket=require('faye-websocket');
const etools=require('ewt-etools');

// 从req中提取preSession
function getHttpBuildPreSession(req){
  const preSession={
    SessionId:etools.getCookie(req && req.headers && req.headers.cookie).SessionId,
    host:req.headers.host
  }
  return preSession
}

// session管理类
class c_sessionManage{
  constructor(httpio){
    this.httpio=httpio;
    this.w=httpio.w;
    this.AccountObjDict={}; // 用户信息对象内存缓存
    this.SessionIdDict={}; // 在线Session列表，key为SessionId，值为数组：[0] 为过期毫秒时间戳; [1]为AccountId
  }

  // 初始生成新session
  newSession(){
    // 生成唯一字符串
    let SessionId;
    do{
      SessionId=etools.ranStr(20)
    }while(SessionId in this.SessionIdDict);

    // 生成初始信息并加入字典
    this.SessionIdDict[SessionId]=[
      Date.now()+this.httpio._conf.sessionLife*1000,
      null
    ];

    // 返回生成的字符串
    return SessionId

  }

  // 检查SessionId，并更新有效期。有效，则意味着SessionId对应的arry可用
  isOkSessionId(SessionId){
    // 没有SessionId，必然false
    if(!SessionId) return false;
    // 获取该SessionId下的数组
    const arry=this.SessionIdDict[SessionId];
    // 若不存在，反馈失败
    if(!arry) return false;
    // 验证在有效期
    const now=Date.now()
    if(arry[0]<now){
      // 过期，销毁
      this.SessionOff(SessionId);
      // 返回false
      return false
    }
    // 有效，延长有效期
    arry[0]=now+this.httpio._conf.sessionLife*1000;
    // 返回true
    return true
  }

  // 由SessionId获取AccountObj
  getAccountObj_obj(SessionId){
    // 若SessionId无效，反馈null
    if(!this.isOkSessionId(SessionId)) return null;

    // 若未登录，反馈空对象
    const AccountId=this.SessionIdDict[SessionId][1];
    if(!AccountId) return {}

    // 从AccountObj字典中反馈，若字典中没有，也反馈空对象
    return this.AccountObjDict[AccountId] || {};
  }

  // 由AccountId获取SessionIds字符串数组
  getSessionIds(AccountId){
    // 如果该帐号对象不存在，则必然没有任何会话，不必继续。该语句只为提高性能。
    if(!AccountId || !(AccountId in this.AccountObjDict)) return [];
    // 遍历取出符合条件的
    const result=[];
    for(const SessionId in this.SessionIdDict){
      const arry=this.SessionIdDict[SessionId];
      if(arry[1] === AccountId) result.push(SessionId);
    }
    // 如果该AccountId取出符合条件数量为0，也就没用了
    if(result.length===0) delete this.AccountObjDict[AccountId];
    // 返回符合条件的数组
    return result;
  }

  // 为SessionId设置、更新AccountObj，必须含有AccountId项目，其他项任意
  SessionLogin_errStr(SessionId,AccountObj){
    // 验证帐号对象可用
    if(typeof AccountObj!=='object' || typeof AccountObj.AccountId!=='string')
      return 'SessionLogin Object without AccountId';
    // 判断该SessionId依然有效
    if(!this.isOkSessionId(SessionId))
      return 'SessionId cannot use'

    // 记录帐号信息
    this.AccountObjDict[AccountObj.AccountId]=AccountObj;
    // 登记Session下的AccountId
    this.SessionIdDict[SessionId][1]=AccountObj.AccountId;
    // 触发消息
    this.httpio._emitMsg(`AccountId: ${AccountObj.AccountId}, loged  Session(${SessionId})`)
  }

  // 会话退出登录(SessionId可继续复用)
  SessionLogout_errStr(SessionId){
    // 验证该SessionId是否可用
    if(!this.isOkSessionId(SessionId)) return 'SessionId cannnot use';

    // 如果之前就没有关联AccountId，不用任何处理
    if(!this.SessionIdDict[SessionId][1]) return;

    const AccountId=this.SessionIdDict[SessionId][1];

    // 取消AccountId关联
    this.SessionIdDict[SessionId][1]=null;

    // 触发消息事件
    this.httpio._emitMsg(`AccountId: ${AccountId}, logout Session(${SessionId})`)
  }

  // 销毁SessionId
  SessionOff(SessionId){
    // 不存在的SessionId，直接退出
    if(!SessionId || !(SessionId in this.SessionIdDict)) return;

    // 关断WebSocket连接
    this.httpio.w.SessionIdOff(SessionId);

    // 删除会话信息
    delete this.SessionIdDict[SessionId]
  }

  // 销毁用户所属的全部Session
  AccountOff(AccountId){
    this.getSessionIds(AccountId).forEach((SessionId)=>{
      this.SessionOff(SessionId);
    })
    // 删除AccountId对象
    delete this.AccountIdDict[AccountId]
  }
}

// ws实例管理类（在httpio内部建立）
class c_wsManage{
  constructor(){
    this.WsIdDict={}      // 在线webSocket连接字典，其值是ws实例
    this.SessionIdDict={} // Session字典，其值是下属WsId数组
  }
  // 新WebSocket链接分配Id，此时ws已有SessionId
  wsRegToSession(ws){
    // 生成唯一的wsid键值
    let wsid;
    do{
      wsid=etools.ranStr(10)
    }while(wsid in this.WsIdDict);

    // 登记到ws字典
    this.WsIdDict[wsid]=ws;

    // 登记到SessionId对应的数组中
    if(!this.SessionIdDict[ws.SessionId])
      // 之前为空
      this.SessionIdDict[ws.SessionId]=[wsid];
    else
      // 之前已有
      this.SessionIdDict[ws.SessionId].push(wsid);

    // 返回wsid
    return wsid
  }

  // WebSocket连接关闭
  WsOff(WsId){
    let SessionId;
    // WsIdDict 操作
    const ws=this.WsIdDict[WsId];
    if(ws){
      SessionId=ws.SessionId; // 记录SessionId
      ws.close(); // 关闭链接
      delete this.WsIdDict[WsId] // 删除字典记录
    }

    const SessionArry=SessionId && this.SessionIdDict[SessionId];
    // Session下属操作
    if(SessionArry){
      // 找WsId位置
      const index=SessionArry.indexOf(WsId);
      // 找到
      if(index>=0){
        // 移除
        SessionArry.splice(index,1);
        // 如果移除之后为空数组，删除
        if(SessionArry.length===0) delete this.SessionIdDict[SessionId];
      }
    }
  }

  // 关闭SessionId下的所有Ws链接
  SessionIdOff(SessionId){
    // 关断WebSocket连接
    const SessionArry=this.SessionIdDict[SessionId];
    SessionArry && SessionArry.forEach((WsId)=>{
      this.WsOff(WsId)
    });
  }
}

class c_httpio extends etools.classBase{
  /* 等待外部定义的方法: */
  // apiReq: 处理api请求的方法
  // wsPreBuild: ws连接预备建立的方法

  constructor(conf){
    super({
      listenPort:null,
      apiUrlHead:'/api',
      wsUrlHead:'/websocket',
      staticPath:null,
      staticDefaultDoc:'index.html',
      apiReqMaxLength:2*1024*1024,
      apiReqAlowNoContentLength:true,
      sessionUrl:null, // null时，为自身管理
      sessionDomain:null,// null时，直接使用host
      sessionLife:1200,
      wsapiTimeout:15,
      staticMime:{
        "css": "text/css",
        "gif": "image/gif",
        "html": "text/html",
        "ico": "image/x-icon",
        "jpeg": "image/jpeg",
        "jpg": "image/jpeg",
        "js": "text/javascript",
        "json": "application/json",
        "pdf": "application/pdf",
        "png": "image/png",
        "svg": "image/svg+xml",
        "swf": "application/x-shockwave-flash",
        "tiff": "image/tiff",
        "txt": "text/plain",
        "wav": "audio/x-wav",
        "wma": "audio/x-ms-wma",
        "wmv": "video/x-ms-wmv",
        "xml": "text/xml",
        "ttf": "font/otf",
        "woff": "application/x-font-woff",
        "woff2": "application/x-font-woff"
      },
      ipUserArry:null,
    });
    this.setConf(conf);
  }
  // 设定方法
  setConf(conf){
    // 状态检测
    if(this.status!=='stop'){
      throw new Error('only setConf at STOP status');
    }

    // 如果有配置传入
    if(conf){
      // 调用内部更改方法
      this._renewConf(conf);
      // 如果有更新ipUserArry，则内部调用更新
      if('ipUserArry' in conf) this._ipUserArryFormat();
    }
  }
  
  // ipUser初始化方法
  _ipUserArryFormat(){
    const ipUsers=this._conf.ipUserArry;
    // 如果没有设定，则匹配数组也为null
    if(!ipUsers) {this._ipMatchArry=null; return }

    // 整理，根据优先级，整理为适合使用的数组，目的为加速
    const arry=this._ipMatchArry=[];// 三项分别为：ipUser ipraw mask
    for(let i=0;i<ipUsers.length;i++){
      const user=ipUsers[i];
      for(let j=0;j<user.ips.length;j++){
        const a=user.ips[j].split('/');
        const ipraw=etools.ip2raw(a[0]);
        let mask;
        // 未设置，要求全部匹配
        if(!a[1]) mask=128;
        // ipv6
        else if(a[0].indexOf(':')>=0) mask=Number.parseInt(a[1]);
        // ipv4，需要补充与ipv6差的96位
        else mask=Number.parseInt(a[1])+96;
        // 添加
        arry.push([user.ipUser,ipraw,mask])
      }
    }
  }
  
  // ipUser匹配方法
  ipUserMatch(ipstr){
    // 没有ip提供时，反馈no-IP
    if(!ipstr) return 'no-IP'
    
    const ipraw=etools.ip2raw(ipstr);
    const arry=this._ipMatchArry;
    for(let i=0;i<arry.length;i++){

      const item=arry[i];
      // 检索在mask要求范围内是否全部匹配
      let isOk=true;
      const full=Math.floor(item[2]/8);
      const last=item[2]%8;

      // 非最后一位
      let j;
      for(j=0;j<full;j++){
        if(ipraw[j]!==item[1][j]){
          isOk=false;
          break
        }
      }
      // 最后剩余的按位判断
      if(isOk&&last){
        const move=8-last;
        if((ipraw[j]>>move)!==(item[1][j]>>move)) isOk=false;
      }
      // 如果匹配成功，反馈该ipUser对象
      if(isOk) return item[0];
    }
  }
  
  // 启动方法
  _boot(callback){
    // ws会话管理
    this.w=new c_wsManage();
    
    // Session管理
    this.s=this._conf.sessionUrl || new c_sessionManage(this);

    // 创建http实例
    this._httpServer=HTTP.createServer();

    // 关联事件
    this._httpServer.on('request',(req,res)=>{
      this._httpServer_on_request(req,res);
    });
    this._httpServer.on('upgrade',(req,socket,body)=>{
      this._httpServer_on_upgrade(req,socket,body);
    });
    this._httpServer.on('close',()=>{
      this.stop()
    });

    // 启动, idResult
    this._httpServer.listen(this._conf.listenPort,(err,data)=>{
      callback(err);
    });
  }
  // 停止方法
  _stop(callback){
    // 关闭http监听
    this._httpServer.close(()=>{
      // 成功后清空各种内部实例
      this._httpServer=null;

      callback()
    })
  }

  // 识别函数，带出识别信息
  /*
  * preSession：
  *   cmd       要进行的操作，默认为识别：recognize
  *   SessionId 未验证的SessionId
  *   host      请求来源的host
  *   failRenew 若失败是否生成新SessionId
  * */
  sessionFunc(preSession,callback) {
    /* 根据是否定义了s，来确定委托还是自身管理Session */
    if(this.isSessionRoot){
      // 反馈结果对象构造
      const result={
        SessionId:null,  // 最终输出时，必须是有效SessionId
        AccountObj:null, // 最终输出时，不能为null，可以是空对象
        setCookie:null   // 为null表示不需要让前端更新cookie，有值时为更新cookie用的字符串
      };

      // 获取AccountObj，该过程中也校验了SessionId是否有效，有效时延长有效期
      const AccountObj=this.s.getAccountObj_obj(preSession.SessionId);

      // 获取到AccountObj，表明Session有效，但可能AccountObj为空对象，即未登录
      if(AccountObj) {
        result.SessionId=preSession.SessionId;
        result.AccountObj=AccountObj;
      }
      // AccountObj到为null，若要求重新颁发，则申请新SessionId
      else if(preSession.failRenew){
        // 生成新SessionId，此时AccountObj必然是空
        result.SessionId=this.s.newSession();
        result.AccountObj={};

        /* 判定生成的domain，如果为空，则不设定 */
        let domain

        let domainArry=this._conf.sessionDomain; // 配置信息
        // 有设置，进行规则匹配；没有，则允许domain为空
        if(domainArry){
          // 如果没有host报错
          if(!preSession.host){
            callback('sessionDomain need host in preSession');
            return;
          }
          // 包装成数组
          if(!Array.isArray(domainArry)) domainArry=[domainArry];
          // 遍历
          for(let i=0;i<domainArry.length;i++){
            if(preSession.host.indexOf(domainArry[i])>=0){
              domain=domainArry[i];
              break;
            }
          }
        }

        const cookie=[
          'SessionId='+result.SessionId,
          'path=/',
          'expires=Thu, 31-Dec-37 23:55:55 GMT',
          'max-age=2147483647',
          'HttpOnly'
        ];
        // 是否设置了共享域
        if(domain) cookie.push('Domain='+domain);

        result.setCookie=cookie.join('; ');
      }
      // AccountObj到为null，且不重新颁发，无需改动result

      // 回调
      callback(undefined,result);
    }
    // s为URL字符串，调用该URL委托管理，调用API请求
    else etools.ajax({
      url:this.s,
      post:preSession
    },(err,json)=>{
      // 请求异常
      if(err ||
        (typeof json!=='object')||
        (typeof json.body!=='object')
      ) callback(err || 'ajax result: no json.body');
      // 请求正常
      else callback(undefined,json.body);
    })
  }
  get isSessionRoot(){return typeof this.s==='object'}
  
  // =========== http收到request请求 ===========
  _httpServer_on_request(req,res){
    const urlInfo=URL.parse('http://'+req.headers.host+req.url,true);
    // 判定是否是API请求
    if(urlInfo.pathname.substr(0,this._conf.apiUrlHead.length)===this._conf.apiUrlHead){
      this._request_API(urlInfo,req,res)
    }
    // url.pathname中没有"."且不是根路径，则也认为是API请求，自动补充API前缀位
    else if(!urlInfo.pathname.includes('.')&&urlInfo.pathname!=='/'){
      urlInfo.pathname=this._conf.apiUrlHead+urlInfo.pathname;
      this._request_API(urlInfo,req,res)
    }
    // 不是API请求，则是文件请求
    else {
      this._request_Static(urlInfo,req,res)
    }
  }
  _request_API(urlInfo,req,res){
    // 允许跨域
    res.setHeader('Access-Control-Allow-Origin', '*');

    // 移除url中api标识的前缀
    urlInfo.pathname=urlInfo.pathname.slice(this._conf.apiUrlHead.length);

    // 构造api请求包
    const apiReqJson={
      from: 'http',
      httpHead: req.headers,
      method: req.method,
      api: urlInfo.pathname.substr(1),
      params: urlInfo.query || {},
      host: urlInfo.hostname,
      reqMime: req.headers['content-type'],
      resMime: req.headers['accept'] || 'application/json'
    };

    // 获取ip地址
    apiReqJson.ip=
      req.headers['x-forwarded-for'] ||
      req.connection.remoteAddress ||
      req.socket.remoteAddress ||
      req.connection.socket.remoteAddress;

    // 获取ipUser
    if(apiReqJson.ip) apiReqJson.ipUser=this.ipUserMatch(apiReqJson.ip)
    
    // 构造反馈函数
    const callback=(err,json,raw,headers)=>{
      switch (typeof headers) {
        case 'string':// 字符串，按setCookie设置
          res.setHeader('Set-Cookie', headers);
          break
        case 'object':// 对象，逐一遍历
          for(const i in headers){
            res.setHeader(i, headers[i]);
          }
          break
      }

      // 整理反馈类型，流
      let ctype,finalRaw;
      try{
        // 有错误发生
        if(err) [ctype,finalRaw]=etools.jr2mime_catch(apiReqJson.resMime,err)
        // 没有错误
        else [ctype,finalRaw]=etools.jr2mime_catch(apiReqJson.resMime,json,raw)
      } catch (e) {
        console.log('do res MIME Error:' +apiReqJson.resMime)
        console.log(e)
        res.end();
        return;
      }

      res.setHeader('Content-Length',finalRaw.length); // 设置文档长度
      res.setHeader('Content-Type', ctype); // 设定文档类型
      res.writeHead(err?400:200); // 设置状态码，必须在setHeader之后



      // 发送最终流
      res.end(finalRaw)
    };

    // 从req中提取Session识别信息
    const preSession=getHttpBuildPreSession(req);
    
    // 根据method选择处理
    switch (apiReqJson.method){
      // 直接触发事件
      case 'GET':
      case 'DELETE':
        apiReqJson.post={}; // 兼容性需要
        this.apiReq(preSession,apiReqJson,undefined,callback);
        break;
      // 接收数据体后，触发事件
      case 'POST':
        const rawArry=[];

        // content-length字段处理
        let errObj=null;
        try{
          const contentLength=Number.parseInt(req.headers['content-length']);
          if(contentLength>this._conf.apiReqMaxLength) errObj={state:'fail',msg:'contentLength is too long'};
        } catch (e){
          // 获取长度失败
          if(!this._conf.apiReqAlowNoContentLength) errObj={state:'fail',msg:'httpHeader must content content-length'};
        }
        // try中有错误，反馈结束
        if(errObj){
          callback(errObj);
          return;
        }

        let length=0;
        let tooLong=false;
        // 读取数据
        req.addListener('data',(data)=>{
          // 累计长度，并验证是否超长
          length+=data.length;
          if(length>this._conf.apiReqMaxLength){
            callback({state:'fail',msg:'contentLength is too long'});
            req.client.destroy();
            tooLong=true;
            return
          }
          // 数据写入数组
          rawArry.push(data)
        });
        // 调用api执行
        req.addListener('end',()=>{
          if(tooLong) return; // 如果之前已经因为超长报错，则不需要执行此处
          const raw=Buffer.concat(rawArry);  // 合并为完整Buffer
          apiReqJson.rawLength=raw.length;   // post请求发过来的长度
          // 根据Content-Type识别二进制流
          let post,innerRaw;
          try{
            [post,innerRaw]=etools.mime2jr_catch(apiReqJson.reqMime,raw)
          } catch (e){
            console.log('do req MIME Error:' +apiReqJson.reqMime)
            console.log(e)
            callback({state:'fail',msg:'do MIME Error: '+apiReqJson.reqMime});
            return
          }
          // 给post项赋值
          apiReqJson.post=post
          // 调用API执行
          this.apiReq(preSession,apiReqJson,innerRaw,callback);
        });
        break;
      default:
        callback('undo method: '+req.method+', url: '+urlInfo.url);
        this._emitMsg('undo method: '+req.method+', url: '+urlInfo.url)
    }
  }     // API请求
  _request_Static(urlInfo,req,res){
    // 从req中尝试识别SessionId
    const SessionId=etools.getCookie(req && req.headers && req.headers.cookie).SessionId;

    // 如果没有配置静态路径，则不响应静态文件请求，反馈400
    if(!this._conf.staticPath){
      res.writeHead(400, {'Content-Type': 'text/plain'});
      res.end('no static resource to access');
      this._emitMsg(`STATIC (400) no static resource: ${urlInfo.href}, Session: ${SessionId}`);
      return
    }

    // 是否需要补充默认文档
    if(urlInfo.pathname.substr(urlInfo.pathname.length-1)==='/')
      urlInfo.pathname+=this._conf.staticDefaultDoc;

    // 请求实际文件路径
    const filePath=this._conf.staticPath+urlInfo.pathname;

    // 获取请求文件对应的MIME类型，若获取不到，则404
    let mimeType;
    const ext=path.extname(filePath).slice(1);
    if(ext in this._conf.staticMime){
      // 匹配成功
      mimeType=this._conf.staticMime[ext]
    }
    else {
      // 匹配失败
      res.writeHead(404, {'Content-Type': 'text/plain'});
      res.write("unknow file type: " + urlInfo.pathname);
      res.end();
      this._emitMsg(`STATIC (404) no MIME: ${urlInfo.href}, Session: ${SessionId}`);
      return
    }

    fs.stat(filePath,(err)=>{
      // 该文件不存在，反馈404错误
      if (err) {
        res.writeHead(404, {'Content-Type': 'text/plain'});
        res.write("This request URL " + urlInfo.pathname + " was not found on this server.");
        res.end();
        this._emitMsg(`STATIC (404) no file: ${urlInfo.href}, Session: ${SessionId}`);
        return
      }

      // 存在则读取
      fs.readFile(filePath,'binary',(err,buf)=>{
        // 读取失败，则反馈500错误
        if(err){
          res.writeHead(500, {'Content-Type': 'text/plain'});
          res.end(err);

          this._emitMsg(`STATIC (500) readError: ${urlInfo.href}, Session: ${SessionId}`);
          return
        }

        // 读取成功
        res.writeHead(200, {
          'Content-Type' : mimeType,
          'Cache-Control': 'max-age=259200', // 缓存
          'Expires':(new Date(Date.now()+30*86400*1000)).toUTCString()// 缓存
        });
        res.write(buf, "binary");
        res.end();

        // 触发消息
        this._emitMsg(`STATIC Success: ${urlInfo.href}, Session: ${SessionId}`);

      })
    })
  }  // 文件请求

  // =========== http收到webSocket连接请求 ===========
  _httpServer_on_upgrade(req,socket,body){
    // 判定是否是ws连接，不是则终止
    if(!WebSocket.isWebSocket(req)) return;

    // 从req中提取Session识别信息
    const preSession=getHttpBuildPreSession(req);
    preSession.failRenew=true; // 需要一定有效
    
    // 验证或重新分配SessionId
    this.sessionFunc(preSession,(err,idResult)=>{
      // 如果是新分配的SessionId，写头信息
      let options;

      // Session处理流程出错
      if(err){
        socket.end();
        return
      }

      // 需要重新设置根据ws组件文档 https://www.npmjs.com/package/faye-websocket
      if(!err && idResult && idResult.setCookie) options={
        headers:{ 'Set-Cookie': idResult.setCookie }
      }

      // WebSocket实例生成
      const ws=new WebSocket(req,socket,body,null,options);

      /* WebSocket实例确定可用 */

      // 记录SessionId，分配WsId
      ws.SessionId=idResult.SessionId
      ws.WsId=this.w.wsRegToSession(ws);
      ws.host=preSession.host;

      // 触发消息
      this._emitMsg(`WebSocket On , SessionId: ${ws.SessionId}, WsId: ${ws.WsId}, path:${req.url}`);

      // 收到jr消息
      ws.on('message',(event)=>{
        let json,raw;
        // 解析失败则忽略该消息
        try{
          [json,raw]=etools.buffer2jr(Buffer.from(event.data))
        } catch(e) {
          this._emitWarn(1,'websocket message JR type Error'+event.data.toString());
          return
        }

        // 没有json的消息，无法继续
        if(!json){
          this._emitWarn(1,'websocket message without Json'+event.data.toString());
          return
        }

        // json一级cmd解析
        switch (json.cmd){
          case '_heart':
            ws.send_jr({cmd:'_heartBak'});
            break;
          case 'api'   : this._ws_request_API(json,raw,ws); break;
          case 'apiBak': this._ws_wsapiBak(json,raw,ws); break;
          default:
            this._emitWarn(1,'rec undo WebSocket jr: '+JSON.stringify(json))
        }
      });
      // 断开
      ws.on('close',(event)=>{
        this._emitMsg(`WebSocket Off, SessionId: ${ws.SessionId}, WsId:${ws.WsId}`);
        this.w.WsOff(ws.WsId);
      });
      // 发送jr消息
      ws.send_jr=(json,raw)=>{
        // 如果ws已关闭，则无法发送
        if(!ws) return;

        // 执行发送
        ws.send(etools.jr2buffer(json,raw))
      };

      // 获取ip地址
      ws.httpIp=
        req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress;

      // 获取ipUser
      if(ws.httpIp) ws.ipUser=this.ipUserMatch(ws.httpIp)
    })
  }
  _ws_request_API(json, raw, ws){
    // 构造api请求包
    const apiReqJson={
      from: 'ws',
      method: json.method || (json.post?'POST':'GET'),
      api: json.api,
      ip: ws.httpIp,
      ipUser:ws.ipUser,
      params: json.params|| {},
      post: json.post || {},
      WsId: ws.WsId
    };
    // 有json类型的post时
    if(json.post){
      apiReqJson.post=json.post;
      apiReqJson.rawType='json'
    }
    // 有raw时
    else if(raw) apiReqJson.rawType='raw';

    // 组织反馈对象
    const resObj={
      cmd:'apiBak',
      count:json.count
    };
    // 组织回调函数
    const callback=(err,json,raw,sessionCheckErr)=>{
      // 如果有setCookie，说明Session已经失效，断开该链接
      if(sessionCheckErr){
        ws.close(3001);
        return;
      }
      // 正常处理
      resObj.err =err;
      resObj.json=json;
      ws.send_jr(resObj,raw);
    };
    
    // 触发外部apiReq事件
    this.apiReq({
      SessionId:ws.SessionId,
      host:ws.host
    },apiReqJson,undefined,callback);
  }
  _ws_wsapiBak(json, raw, ws, idResult){
    const count=json?json.count:null;
    // 没有序号，或服务端没有反馈集
    if(count===null||
      !ws.callbackFunc||
      !(count in ws.callbackFunc))
    {
      this._emitMsg('rec wsapi back, but timeout, sessionId: '+ws.SessionId+'  WsId:'+ws.WsId);
      return;
    }
    // 调用反馈
    ws.callbackFunc[count](json.err,json.json,raw);
    // 从移除反馈集中移除
    delete ws.callbackFunc[count];
  }

  // 对外属性：
  get listenPort(){
    if(this.status==='work'){
      return this._conf.listenPort;
    } else {
      return null
    }
  }   // 当前监听端口

  // 对外方法，调用指定前端wsApi
  wsapi(WsId,apiName,json,raw,callback){
    const ws=this.w.WsIdDict[WsId];

    // 若WsId不可用，则反馈错误
    if(!ws){
      callback({ecode:10200002,msg:'WsId: '+WsId});
      // 返回
      return
    }

    // 请求序号及缓存初始化
    if(typeof ws.apiNum==='undefined'){
      ws.apiNum=0;
      ws.callbackFunc={};
    }

    // 序号归零
    if(ws.apiNum>9e9) ws.apiNum=0;

    // 构造请求json
    const reqObj={
      cmd:'api',
      api:apiName,
      post:json,
      count:ws.apiNum++
    };

    // 记录回调函数
    ws.callbackFunc[reqObj.count]=callback;

    // 请求超时触发器
    setTimeout(()=>{
      if(reqObj.count in ws.callbackFunc){
        ws.callbackFunc[reqObj.count]('timeout');
        delete ws.callbackFunc[reqObj.count];
      }
    },this._conf.wsapiTimeout*1000);

    // 向前端发送WebSocket消息
    ws.send_jr(reqObj,raw)
  }

  /* 对外事件：
  * apiReq  (apiReqJson,raw,callback)
  * */
}

module.exports={
  server:c_httpio,
  sessionManage:c_sessionManage
};