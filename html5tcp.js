'use strict'
var apiTimeOut=20 // 默认api超时时间
/*
[实例化c_emsg配置说明]
实例化中的配置对象优先级高于defaultEmsgConf，
即defaultEmsgConf中为默认配置。默认配置中null
标识必须在实例化时给的项目。
 */

var defaultEmsgConf={
    heartTime:10,           // (秒)心跳包周期，每隔该时间向服务器发送一次
    heartOut:15,            // (秒)心跳超时时间，未收到心跳回复，则认为链路损坏，主动断开
    reConnTime:2,           // 尝试重连周期
    wsPath:'/websocket',    // webSocket连接路径
    ajaxPath:'/api'         // httpAjax请求时的路径
}

// ===== 自定义工具库 =====
var Buffer={
    isBuffer:function(buffer){
        // 没有buffer或不是object，判定不是buffer
        if(!buffer||(typeof buffer!='object')) return false
        // 构造对象必须是Uint8Array
        // IE 不兼容if(buffer.constructor.name!='Uint8Array') return false
        if(!buffer.buffer) return false
        // 没问题，反馈ok
        return true
    },
    toStr:function(buffer,code){
        if(!code)code='utf8'
        if(!Buffer.isBuffer(buffer)) throw new Error('buffer error')
        switch(code){
            case 'utf8':
                var i=0
                var str=''
                // 每次循环出来一个字符
                while (i<buffer.length){
                    var u // 该字符的unicode编码
                    // 1位长度
                    if(buffer[i]<0x80){
                        u=buffer[i]
                        i++
                    }
                    // 2位长度
                    else if(buffer[i]>=0xc0&&buffer[i]<0xe0){
                        u=(buffer[i]%0x20<<6) | buffer[i+1]%0x40
                        i+=2
                    }
                    // 3位长度
                    else if(buffer[i]>=0xe0&&buffer[i]<0xf0){
                        u=(buffer[i]%0x10<<12) | (buffer[i+1]%0x40<<6) | buffer[i+2]%0x40
                        i+=3
                    }
                    // 错误时向后一位，忽略该字节
                    else {
                        i++
                    }
                    str+=String.fromCharCode(u)
                }
                return str
                break
            default:
                throw new Error('unknow code: '+code)
        }
    },
    fromStr:function(str,code){
        if(!code) code='utf8'
        if(typeof str!='string') throw new Error('string error')
        switch(code){
            case 'utf8':
                var arry=[];
                for(var i=0;i<str.length;i++){
                    var u=str.charCodeAt(i);
                    // 1位长度
                    if(u<0x80){
                        arry.push(u)
                    }
                    // 2位长度
                    else if(u<0x800){
                        arry.push(0xc0+(u>>>6));
                        arry.push(0x80+(u%0x40))
                    }
                    // 3位长度
                    else if(u<0x10000){
                        arry.push(0xe0+(u>>>12));
                        arry.push(0x80+(u>>>6)%0x40);
                        arry.push(0x80+u%0x40)
                    }
                }
                return new Uint8Array(arry);
                break;
            default:
                throw new Error('unknow code: '+code)
        }
    },
    alloc:function(len){
        if(!etools.isInt(len) || len<0) throw new Error('length must be Int');
        return new Uint8Array(len)
    },
    concat:function(arry){
        var rArry=[]
        arry.forEach(function(item){
            if(!Buffer.isBuffer(item)) throw new Error('items of arry must be Uint8Array Object')
            for(var i=0;i<item.length;i++)
                rArry.push(item[i])
        })
        return new Uint8Array(rArry)
    }
}   // 二进制流处理函数
var etools={
    // 获取raw中，0所在的位置，反馈数组
    find0:function(raw,start,maxCount){
        if(!Buffer.isBuffer(raw)) throw new Error("input raw isNot Buffer")
        if(maxCount===undefined) maxCount=1 // 默认1
        if(!etools.isInt(maxCount)||maxCount<1)
            throw new Error("input maxCount error")
        if(!etools.isInt(start)||start<0) start=0 // 默认从0位开始
        var rArry=[]
        for(var i=start;i<raw.length;i++){
            if(raw[i]==0) rArry.push(i)
        }
        return rArry;
    },
    // 将一个json对象和一个raw对象合并成一个Buffer
    jr2buffer:function(json,raw){
        if(json!==undefined && (typeof json!='object')) throw new Error('json must be Object')
        if(raw!==undefined && !Buffer.isBuffer(raw)) throw new Error("input raw isNot Buffer")

        json=Buffer.fromStr(JSON.stringify(json),'utf8')
        var mixJsonRaw
        var zeroBuffer=Buffer.alloc(1)
        if(json!==undefined && raw!==undefined){
            // 有 json 和 raw
            mixJsonRaw=Buffer.concat([json,zeroBuffer,raw])
        } else if(json!==undefined){
            // 有json
            mixJsonRaw=Buffer.concat([json])
        } else if(raw!==undefined){
            // 有raw
            mixJsonRaw=Buffer.concat([zeroBuffer,raw])
        } else {
            // 没有json也没有raw
            mixJsonRaw=Buffer.alloc(0)
        }
        return mixJsonRaw
    },
    // 将一个Buffer拆分成一个json对象和一个raw对象
    buffer2jr:function(buffer){
        if(!Buffer.isBuffer(buffer)) throw new Error("input raw isNot Buffer")

        var json,raw;
        // 解析过程
        var arry0=etools.find0(buffer)
        if(arry0.length==0){
            // 没有0位分割，全是json
            json=Buffer.toStr(buffer,'utf8')
        } else if(arry0[0]==0){
            // 0位分割在0位，表示无json，只有raw
            raw=buffer.slice(1)
        } else {
            // 有0位分割，且前后均有数据
            json=(buffer.slice(0,arry0[0])).toString('utf8')
            raw=buffer.slice(arry0[0]+1)
        }
        // json实例化
        try{
            if(json!==undefined) json=JSON.parse(json)
        } catch (e){
            throw new Error("JSON format error")
            return
        }

        return [json,raw]
    },
    // 类型判定int型数字
    isInt:function(num){
        if(typeof num!='number' ||
            num===NaN       ||
            num===Infinity  ||
            num===-Infinity ||
            Math.floor(num) !==num||
            num>9e14||num<-9e14
        ){
            return false
        } else {
            return true
        }
    },
    // 输出指定时间戳对应的字符串
    stamp2fmt:function(stamp,fmt){
        if(typeof stamp=='number')stamp=new Date(stamp)
        if(!fmt)fmt='yyyy-MM-dd hh:mm:ss'
        var o = {
            "M+": stamp.getMonth() + 1, //月份
            "d+": stamp.getDate(), //日
            "h+": stamp.getHours(), //小时
            "m+": stamp.getMinutes(), //分
            "s+": stamp.getSeconds(), //秒
            "q+": Math.floor((stamp.getMonth() + 3) / 3), //季度
            "S": stamp.getMilliseconds() //毫秒
        }
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (stamp.getFullYear() + "").substr(4 - RegExp.$1.length))
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)))
        return fmt
    },
    // 是否是手机号
    isPhoneNum:function(num){
        if(typeof num=='string'){
            try{
                num=parseInt(num)
            }catch (e){
                return false
            }
        }
        if(typeof num!='number') return false

        return etools.isInt(num)&&num>130e8&&num<190e8
    },
    // 从arguments中提取other
    getOther:function(args,start){
        var other=[]
        for(var i=start||0;i<args.length;i++){
            other.push(args[i])
        }
        return other
    },
    // 随机字符串
    ranStr:function(len){
        var $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-_';
        var maxPos = $chars.length;
        var pwd = '';
        for (var i = 0; i < len; i++) {
            pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
        }
        return pwd;
    },
    // 并行任务
    task:function(timeoutSecond){
        var that={}
        // 反馈计数器
        that.out_count=0
        // 返回结果预备数组
        that.rout=[]
        // 超时定时器
        if(timeoutSecond>0)
            that.timeout=setTimeout(function(){
                if(typeof that.final=='function'){
                    that.final(that.rout)
                }
            },timeoutSecond*1000)

        // 添加任务
        that.do=function(func){
            var other=etools.getOther(arguments,1) // 其它附加参数

            // ===== 每次调用 =====

            var count=this.rout.length // 记录当前调用序号
            this.rout.push(null)       // 结果数组增长一位

            var that=this
            // 闭包内定义回调函数
            var callback=function(){
                var all=arguments
                // 将反馈结果记录入结果数组
                that.rout[count]=all
                // 有结果反馈计数+1
                that.out_count++
                // 当有结果计数与调用次数相同时，执行类的反馈
                if(that.out_count==that.rout.length){
                    // 取消定时器
                    clearTimeout(that.timeout)
                    // 调用final
                    that.final(that.rout)
                }
            }

            // 强制异步执行
            setTimeout(function(){
                other.push(callback)
                func.apply(func,other)
            },0)
        }
        return that
    },
    // 判断数组中是否有某个元素
    arryhas:function(arry,item){
        if(!Array.isArray(arry)){
            throw new Error("Error - arry is not Array")
            return
        }
        for(var i=0;i<arry.length;i++)
            if(arry[i]===item) return true
        return false
    },
    // 对象复制
    objCopy:function self(obj){
        switch (typeof obj){
            case 'object':
                var robj
                // 数组递归复制
                if(Array.isArray(obj)){
                    robj=[]
                    for(var i=0;i<obj.length;i++)
                        robj.push(self(obj[i]))
                }
                // 普通对象
                else {
                    robj={}
                    for(var i in obj){
                        robj[i]=self(obj[i])
                    }
                }
                return robj
            // 忽略函数的复制
            case 'function':
                return
            // 默认直接反馈
            default:
                return obj
        }
    }
}   // 工具函数
var c_eEvent={
    createNew:function(){
        var obj={}

        // ====== 构造函数 ======

        // 事件列表
        obj.eventList={}
        // 周期函数列表
        obj.circleArray=[]
        // 启动周期函数
        obj.circle_last=Date.now()
        obj.circleSet=setInterval(function(){
            // 取当前时间
            var now=Date.now()
            // 与上次时间计算差秒
            var inc=Math.floor((now-obj.circle_last)/1000)
            // 上次时间增加整秒数
            obj.circle_last+=inc*1000
            // 遍历各周期函数，判定是否调用
            obj.circleArray.forEach(function(item){
                // 若没有tnow，则归零
                if(item.tnow===undefined)item.tnow=0
                // tnow增加与上次差秒
                item.tnow+=inc
                // 若tnow大于设定的调用秒周期，则执行调用并归零tnow
                if(item.tnow>item.circleSecond){
                    item.func() // 调用
                    item.tnow=0 // 归零
                }

            })
        },1000)

        // ====== 方法 ======
        // 订阅事件
        obj.on=function(eventName,func){
            // 输入检测
            if(typeof eventName!='string'||typeof func!='function')
                throw new Error('input error')

            // 已有订阅
            if(eventName in obj.eventList){
                obj.eventList[eventName].push(func)
            }
            // 未有订阅
            else{
                obj.eventList[eventName]=[func]
            }
        }

        // 触发事件
        obj.emit=function(eventName){
            var other=etools.getOther(arguments,1) // 其它附加参数

            if(!(eventName in obj.eventList)) return // 无订阅
            // 遍历该事件下的注册函数，调用执行
            obj.eventList[eventName].forEach(function(func){
                func.apply(obj,other)
            })
        }
        // 周期触发器
        obj.circle=function(second,func){
            obj.circleArray.push({
                circleSecond:second,
                func:func
            })
        }
        // 销毁函数
        obj.destroy=function(){}

        return obj
    }
} // 事件机制基类

// 自己定义的类，实现自己习惯的方法
var c_emsg={
    createNew:function(setObj) {
        var obj = c_eEvent.createNew() // 继承事件基类

        // ======= 配置信息控制方法 ========
        obj.conf={} // 配置节点
        obj.setConf=function(conf){
            // 接收配置信息
            for(var key in conf){
                obj.conf[key]=conf[key]
            }
        }
        obj.setConf(defaultEmsgConf);   // 写入默认配置
        if(setObj) obj.setConf(setObj); // 构造函数参数入配置

        // ====== 内部变量 ======
        obj.debug= false
        obj._emptySec = 0                   // 没有通讯的空白时间
        obj._closeCode= 0                   // 关闭时的反馈代码，https://developer.mozilla.org/en-US/docs/Web/API/CloseEvent

        if (!etools.isInt(obj.conf.heartTime) || obj.conf.heartTime < 0) {
            throw new Error('HeartTime must be Int')
        }
        obj.online = 0   // 0 未连接; 1 正常 ; 2 连接中 ; 3 断线
        obj.ws = undefined // HTML5连接时的实例

        // ====== 方法 ======
        obj._doBuffer=function(arryBuffer){
            // 空白时间归零
            obj._emptySec=0

            // 处理数据
            var buffer=new Uint8Array(arryBuffer)
            var rt=etools.buffer2jr(buffer)
            var json=rt[0]
            var raw=rt[1]
            if(obj.debug){
                console.log('[DEBUG] emsg._doBuffer emit "rec_jr"')
                console.log('[DEBUG]    json: '+JSON.stringify(json))
                console.log('[DEBUG]    raw.length: '+(raw?raw.length:'null'))
            }
            obj.emit('rec_jr',json,raw)
        } // 收到原始buffer消息，触发rec_jr事件
        obj._boot=function(){
            // ============ 微信中调用 ==============
            if(typeof wx!='undefined'){
                if(!obj.conf.encrypt) throw new Error('weixin must use Encrypt')
                wx.connectSocket({
                    url:obj.wsurl(),
                    fail:function(err){
                        obj.emit('error',1002,'build wx connection Eror')
                    }
                })
                wx.onSocketOpen(function(res){
                    obj.online=1
                    obj.emit('connect')
                })
                wx.onSocketError(function(err){
                    obj.emit('error',1001,err)
                })
                wx._doBuffer(function(data){
                    obj._doBuffer(data)
                })
                wx.onSocketClose(function(){
                    obj.online=3
                    obj.emit('closed')
                })
            }
            // ============ HTML5中调用 ==============
            else{
                // 浏览器支持检验
                if(!WebSocket){
                    obj.emit('error',1003)
                    return
                }

                // 建立连接，各事件处理
                var ws=obj.ws=new WebSocket(obj.wsurl())

                ws.onopen=function(evt){
                    obj._emptySec=0
                    obj.online=1
                    if(obj.debug) console.log('[DEBUG] emsg emit "connect"')
                    obj.emit('connect')
                }
                ws.onclose=function(evt){
                    // 只有之前是正常通讯状态时，才会触发closed事件
                    if(obj.online==1){
                        if(obj.debug) console.log('[DEBUG] emsg emit "closed"')
                        obj.emit('closed')
                    }

                    obj._closeCode=evt.code // 记录关闭代码
                    obj.online=3 // 将状态改为掉线
                }
                ws.onmessage=function(evt){
                    // 根据ws数据类选择不同解析方式，并最终调用_doBuffer方法
                    if(ws.binaryType=='blob'){
                        // 以Blob格式读取原始ws消息，提取为ArrayBuffer格式
                        var reader=new FileReader()
                        reader.addEventListener('loadend',function(){
                            var arryBuffer=reader.result
                            obj._doBuffer(arryBuffer)
                        })
                        reader.readAsArrayBuffer(evt.data)
                    } else if (ws.binaryType=='arraybuffer'){
                        // 传输本就是ArrayBuffer
                        obj._doBuffer(evt.data)
                    } else {
                        // 不应到此，不识别的
                        throw new Error('emsg unknow binaryType: '+ws.binaryType)
                    }
                }
                ws.onerror=function(evt){
                    obj.emit('error',1000,'HTML5: '+JSON.stringify(evt))
                }
            }
        }
        obj.wsurl=function(){
            var port=obj.conf.port?obj.conf.port:(obj.conf.encrypt?443:80);
            return (obj.conf.encrypt?'wss://':'ws://')+obj.conf.host+':'+port+(obj.conf.wsPath?obj.conf.wsPath:'/websocket')
        }
        obj.ajaxurl=function(){
            var port=obj.conf.port?obj.conf.port:(obj.conf.encrypt?443:80);
            return (obj.conf.encrypt?'https://':'http://')+obj.conf.host+':'+port+(obj.conf.ajaxPath?obj.conf.ajaxPath:'/api')
        }
        obj.wType=function(){
            return (typeof wx!='undefined')?'weixin':'html5'
        } // 微信还是html5判定

        // ========= 对外方法 =========


        obj.start=function(host,port,encrypt,wsPath){
            if(obj.online==1||obj.online==2) return // 已经在线则或正在连接中，不执行

            // 替换默认配置
            if(host||port){
                obj.setConf({
                    host:host,
                    port:port,
                    encrypt:encrypt,
                    wsPath:wsPath
                })
            }

            obj.online=2; // 状态置为连接中
            obj._boot() // 调用连接过程
        }
        obj.send_jr=function(json,raw){
            // 在线状态验证
            if(!obj.online==1){
                obj.emit('error',1101)
            }

            // 格式验证
            if(typeof json!='object'){
                obj.emit('error',1100,'json must be Object')
                return
            }
            if(raw&& !Buffer.isBuffer(raw)){
                obj.emit('error',1100,'raw must be ArrayBuffer')
                return
            }

            var buf=etools.jr2buffer(json,raw)

            // 两种发送执行
            switch (obj.wType()){
                case 'weixin':
                    wx.sendSocketMessage({

                    })
                    break
                case 'html5':
                    obj.ws.send(buf)
                    break
            }
        }
        obj.close=function(code){
            // 两种执行关闭的方式
            switch (obj.wType()) {
                case 'weixin':
                    break;
                case 'html5':
                    if(obj.ws) obj.ws.close()
                    break;
            }
        }

        // ========= 周期循环函数 =========
        // 在线心跳维持
        obj.circle(1, function () {
            // 非在线状态没必要维持
            if (obj.online!=1) return;

            // 空白时间+1
            obj._emptySec++;
            if(obj.debug) console.log('[DEBUG] emptySec: '+obj._emptySec)

            // 触发心跳发送条件
            if(obj._emptySec==obj.conf.heartTime) obj.send_jr({cmd: '_heart'})

            // 触发断线条件
            if(obj._emptySec==obj.conf.heartOut)  obj.close()
        })
        // 不在线重连尝试
        obj.circle(obj.conf.reConnTime, function () {
            if (obj.online==3) {
                obj.start()
            }
        })
        return obj
    }
    /* 可触发的事件：
    connect : 连接成功
    error   : 错误
    rec_jr  : 收到jr消息
    closed  : 连接断开
    */
}

var emsg=c_emsg.createNew(defaultEmsgConf)
emsg.on('connect',function(){
    console.log('Emsg Connect')
})
emsg.on('error',function(code,msg){
    console.log('Emsg Error('+code+'): '+msg)
})
emsg.on('rec_jr',function(json,raw){
    // 输入检验
    if(typeof json!='object'||!('cmd' in json)){
        console.log('unknow msg: '+json)
        return
    }
    // 命令解析，如无匹配case，则default尝试eapi中jr_
    switch(json.cmd){
        // 心跳反馈
        case '_heartBak':
            console.log('rec heartBack')
            break
        // 尝试eapi类处理
        default:
            // 有，调用
            if(('jr_'+json.cmd) in eapi){
                eapi['jr_'+json.cmd](json,raw)
            }
            // 没有则写日志
            else {
                console.log('rec unknow JR: '+JSON.stringify(json))
            }

    }



})

var eapi={
    // 错误代码枚举类
    errcode:{
        wsOffLine:-10001,
    },
    // 回调函数缓存集合
    _callbacks:{},
    // 消息序号计数器
    _count:0,
    // 前端api集合
    wsapi:{},


    // 以Ajax方式调用API
    callAjax:function(apiName,params,post,callback){
        this.call(apiName,params,post,callback,2)
    },
    // 以WebSocket方式调用API
    callWs:function(apiName,params,post,callback){
        this.call(apiName,params,post,callback,1)
    },
    // 以任意方式(WebSocket方式优先)调用API
    call:function(apiName,params,post,callback,mode) {
        // mode: 0 任意；1 websocket；2 ajax

        // params post空缺时的处理
        if(typeof params=='function'){
            callback = params;
            params   = undefined;
            post  = undefined
        } else if(typeof post=='function'){
            callback = post;
            post  = undefined
        }

        // 输入检验
        if(typeof apiName!='string' ||apiName.length==0 ||typeof callback!='function' || (params && (typeof params!='object'))) {
            throw new Error('etcp.API call format Error');
            return
        }

        // params检验整理
        if(params){
            if(typeof params!='object') throw new Error('params must be jsonObject');
            for(var key in params){
                var t=typeof params[key]
                if(t!='string'&&t!='number') throw new Error('value of params[] must be string or num')
            }
        }

        // 使用webSocket方式判定条件
        if(emsg.online==1 && mode!=2){
            // 输出对象
            var sendObj={
                cmd:'api',
                api:apiName, // apiName
                method:null, // 仿http请求，POST/GET标记
                params:params||null, // 仿http请求，params参数对象
                post:null,   // POST对象，GET方式时会被删除
                count:null   // 消息序号记录
            }
            var sendRaw=undefined

            // 自定义DELETE或其它方式
            if(typeof post=='string'){
                sendObj.method=post
                delete sendObj.post
            }
            // POST方式
            else if(typeof post=='object'){
                sendObj.method='POST'
                // 本身就是二进制流
                if(Buffer.isBuffer(post)){
                    sendRaw=post
                    delete sendObj.post
                }
                // 尝试以JSON格式理解post内容，不可以则报错
                else{
                    try{
                        var str=JSON.stringify(post)
                        sendObj.post=post
                    }catch (e){
                        throw new Error('etcp.API call format Error2');
                        return
                    }
                }
            }
            // GET方式
            else {
                sendObj.method='GET'
                delete sendObj.post
            }

            // 调用次序计数
            var count=eapi._count++
            sendObj.count=count

            // 记录回调函数
            eapi._callbacks[count]=callback

            // 发送请求
            emsg.send_jr(sendObj,sendRaw)

            // 超时判定定时器
            var timeout=setTimeout(function(){
                if(count in eapi._callbacks){
                    eapi._callbacks[count]({code:2003,msg:'timeout'})
                    delete eapi._callbacks[count]
                }
            },apiTimeOut*1000)
        }
        // 使用ajax方式判定条件
        else if(mode!=1){
            // 整理url，将params信息写入url
            var url=emsg.ajaxurl()+'/'+apiName
            if(params){
                url+='?';
                var paramArry=[];
                for(var key in params)
                    paramArry.push(key+'='+params[key])
                url+=paramArry.join('&')
            }

            var ajax=new XMLHttpRequest()
            if(emsg.debug){
                ajax.onabort=function(a,b,c,d){
                    console.log('[DEBUG] ajax onabort')
                    console.log(a+' '+b+' '+c+' '+d)
                }
                ajax.onerror=function(a,b,c,d){
                    console.log('[DEBUG] ajax onerror')
                    console.log(a+' '+b+' '+c+' '+d)
                }
                ajax.onload=function(a,b,c,d){
                    console.log('[DEBUG] ajax onload')
                    console.log(a+' '+b+' '+c+' '+d)
                }
                ajax.onloadend=function(a,b,c,d){
                    console.log('[DEBUG] ajax onloadend')
                    console.log(a+' '+b+' '+c+' '+d)
                }
                ajax.onloadstart=function(a,b,c,d){
                    console.log('[DEBUG] ajax onloadstart')
                    console.log(a+' '+b+' '+c+' '+d)
                }
                ajax.onprogress=function(a,b,c,d){
                    console.log('[DEBUG] ajax onprogress')
                    console.log(a+' '+b+' '+c+' '+d)
                }
                ajax.onreadystatechange=function(a,b,c,d){
                    console.log('[DEBUG] ajax onreadystatechange')
                    console.log(a+' '+b+' '+c+' '+d)
                }
                ajax.ontimeout=function(a,b,c,d){
                    console.log('[DEBUG] ajax ontimeout')
                    console.log(a+' '+b+' '+c+' '+d)
                }
            }
            // 通讯流类型声明
            var dataType='application/json'; // 默认值

            // 调用
            if(post){
                ajax.open('POST',url,true)
            } else {
                ajax.open('GET',url,true)
            }
            // ajax.setRequestHeader('Content-Type','application/json')
            // 反馈
            ajax.onloadend=function(){
                console.log('ajax Status: '+this.status);

                // 将反馈文本按照json xml识别
                var resType=dataType; // 反馈类别默认值
                if(this.status==200) resType=this.getResponseHeader('Content-Type')||resType; // 正常获取时尝试读取
                resType=resType.sqlit('/')[1]; // 简化，只取/后面的部分

                // 声明反馈变量
                var err,resJson,raw;

                // 将类别与返回代码字符串合一，然后switch
                switch (resType+'-'+this.status){
                    // 请求失败
                    case 'json-0':
                        // 生成错误json
                        err={status:0,msg:'net::ERR_CONNECTION_REFUSED'};
                        break;
                    // 请求成功
                    case 'json-200':
                        // 请求成功
                        resJson=JSON.parse(this.responseText);
                        break;
                    // 错误代码
                    default:
                        try{
                            err=JSON.parse(this.responseText);
                        } catch (e) {
                            err=this.responseText;
                        }

                }
                callback(err,resJson,raw)
            }

            if(post){
                ajax.send(Buffer.fromStr(JSON.stringify(post)))
            } else {
                ajax.send()
            }
        }
        // 没有可调用的请求方式
        else{
            // 目前只会因为指定ws方式且不在线
            callback(eapi.errcode.wsOffLine)
        }
    },
    // jr消息处理 - api执行反馈
    jr_apiBak:function(json,raw){
        // 如果回调函数集合中有处理函数，则调用
        // 没有，则忽略该反馈
        // 如果请求超时，则会提前删除回调函数导致此处没有。
        // 后端反馈的json中，json.json为业务对应的object
        if(json.count in eapi._callbacks){
            // 调用回调函数
            eapi._callbacks[json.count](json.err,json.json,raw)
            // 删除回调函数
            delete eapi._callbacks[json.count]
        }
    },

    // jr消息处理 - 前端api调用
    jr_api:function(json,raw){
        // 组织反馈对象
        var resObj={
            cmd:'apiBak',
            count:json.count
        }
        // 该api处理函数未定义，报错
        if(!(json.api in this.wsapi)||(typeof this.wsapi[json.api]!='function')){
            resObj.err={ecode:10201001,msg:'no API: '+json.api}
            console.log(JSON.stringify(resObj.err))
            // count小于0表明不需要反馈
            if(resObj.count<0) return
            // 反馈
            emsg.send_jr(resObj)

        }
        // 调用执行
        else {
            this.wsapi[json.api](json.post,raw,function(err,json,raw){
                // count小于0表明不需要反馈
                if(resObj.count<0) return

                resObj.err=err
                resObj.json=json
                // 反馈
                emsg.send_jr(resObj,raw)
            })
        }
    }
}

// ========== 控件说明 ===========

/*
emsg类:
error事件code参数说明
1000    微信连接内部原生错误
1001    HTML5内部原生错误
1002    微信建立连接失败
1003    浏览器不支持WebSocket
1100    发送jr消息时格式错误
1101    发送jr消息时，客户端不在线
online属性值：
0   未连接
1   正常通讯
2

eapi.call函数:
2000    emsg不在线，无法调用api
2001    调用格式错误
2002    post信息不是二进制流，也不是Object
2003    api调用超时


*/
